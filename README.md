## Deploy to sandbox
```
# install serverless plugins
npm install

# login
aws sso login --profile your-sandbox-profile

# set your profile and save your temporary credentials
export AWS_PROFILE=your-sandbox-profile
aws-sso-cred-restore --profile $AWS_PROFILE

# deploy
serverless deploy --stage sandbox --region us-east-2
```

## Undeploy to sandbox
```
serverless remove --stage sandbox --region us-east-2
```