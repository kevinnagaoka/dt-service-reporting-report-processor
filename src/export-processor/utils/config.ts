import * as env from 'env-var';

export const config = {
    S3_BUCKET: env.get('S3_BUCKET').default('dt-service-reporting-reports').asString(),
};
