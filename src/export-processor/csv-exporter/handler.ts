import { Parser } from 'json2csv';
import { S3 } from 'aws-sdk';
import { config } from '../utils/config';
import { SQSEvent } from 'aws-lambda/trigger/sqs';
import { SQSMessageAttributes } from 'aws-lambda';

export const s3 = new S3();
export function getBodyObject(recordBody) {
    try {
        return JSON.parse(recordBody);
    } catch (error) {
        return null;
    }
}

// Handler to consume Export SQS event that contains report message
export const csvExporter = async (event: SQSEvent) => {
    try {
        for (const record of event.Records) {
            const messageAttributes: SQSMessageAttributes = record.messageAttributes;
            const messageObject = getBodyObject(record.body);
            const reportId = messageObject != null ? messageObject.reportId : -1; 
            
            if(reportId > 0) {
                const reportFileName = 'payment_transaction';
                const csvReportkey = `files/${reportId}/${reportFileName}.csv`;
                const fileKey = `raw-data/${reportId}/${reportFileName}.json`
       
                const content = await s3
                        .getObject({ Bucket: config.S3_BUCKET, Key: fileKey })
                        .promise();
                
                const reportData = JSON.parse(content.Body.toString());
                const reportHeaders = reportData.columns;

                const rows = reportData.rows;
                const parserObjects = [reportHeaders, ...rows]
                let csv = 'No Data';
                
                if (rows.length > 0) {
                    const parser = new Parser( { header: false , includeEmptyRows: false });
                    csv = parser.parse(parserObjects);
                }

                await s3.putObject({ Bucket: config.S3_BUCKET, Key: csvReportkey, Body: csv }).promise();
            }
        }
        return {
            statusCode: 200,
        };
    }
    catch(error) {
        console.log(error);
    }   
};
