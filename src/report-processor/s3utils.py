import boto3
import json
import logging
import os
import unicodedata
import re
import pandas as pd
from botocore.exceptions import ClientError

logger = logging.getLogger()
logger.setLevel(logging.INFO)

s3bucket = "dt-service-reporting-reports"
rawdatapath = "raw-data"
pivotdatapath = "pivot-data"
tmppath = "tmp"

def dataframe_to_file(id, name, dataframe, path):
    filename = "{}.json".format(slugify(name))
    objectpath = "{}/{}/{}".format(path, id, filename)
    try:
        # lambdas have 512mb of non-persistent disk space in the /tmp directory see: https://aws.amazon.com/lambda/faqs/
        tmpfile = "/tmp/{}".format(filename)
        data = json.dumps({"table_name": name, "columns": dataframe.columns.tolist(), "rows": dataframe.values.tolist()})
        jsonfile = open(tmpfile, 'w')
        jsonfile.write(data)
        jsonfile.close()
        upload_file(tmpfile, s3bucket, objectpath)
        os.remove(tmpfile)
    except BaseException as err:
        logger.error("An unexpected error type %s occurred generating csv for table: %s with report id: %s", type(err), name, id)
        raise RuntimeError("Failed to generate csv with unexpected error")

def upload_file(file_name, bucket, object_name):
    s3client = boto3.client("s3")
    try:
        response = s3client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True

# From the django project: https://github.com/django/django/blob/main/django/utils/text.py#L400
# returns a filesystem compliant file name
def slugify(value, allow_unicode=False):
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize("NFKC", value)
    else:
        value = (
            unicodedata.normalize("NFKD", value)
            .encode("ascii", "ignore")
            .decode("ascii")
        )
    value = re.sub(r"[^\w\s-]", "", value.lower())
    return re.sub(r"[-\s]+", "-", value).strip("-_")