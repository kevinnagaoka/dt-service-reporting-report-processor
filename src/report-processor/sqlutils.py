import logging
import sys
sys.path.insert(0, 'vendor')
import pymysql
from .utils import *

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def connect_to_db():
    try:
        hostname = "zeng-reporting-test.cluster-ceyl3eblljmy.us-east-2.rds.amazonaws.com"
        username = "admin"
        password = "password"
        dbname = "Reporting_Sandbox"
        logger.info("Attempting connection to database: %s", dbname)
        return pymysql.connect(host=hostname, user=username, passwd=password, db=dbname, connect_timeout=5)
    except pymysql.MySQLError as e:
        logger.error("Could not connect to MySQL instance")
        logger.error(e)

def update_report_last_build_success(id, timestamp):
    query = "UPDATE metadata SET report_last_build_at = '{}', report_status = '{}' where id = {}".format(timestamp, "Done", id)
    execute_query(query)

def update_report_last_build_failure(id):
    query = "UPDATE metadata SET report_status = '{}' where id = {}".format("Failed", id)
    execute_query(query)

def execute_query(query):
    conn = connect_to_db()
    with conn.cursor() as cur:
        logger.info("Executing query: %s", query)
        cur.execute(query)
    conn.commit()