import time
import numpy as np
from datetime import datetime

agg = {
    "count": np.count_nonzero,
    "max": np.max,
    "mean": np.mean,
    "min": np.min,
    "sum": np.sum
}

# returns the current time in milliseconds since epoch
def current_milli_time():
    return round(time.time() * 1000)

# returns the current time date time format for sql
def current_datetime_time():
    timestamp = time.time()
    return datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')