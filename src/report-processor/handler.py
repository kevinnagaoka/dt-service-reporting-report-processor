import json
import numpy as np
import pandas as pd
import requests
import logging
import os
from .utils import *
from .ddbutils import *
from .s3utils import *
from .sqlutils import *

logger = logging.getLogger()
logger.setLevel(logging.INFO)   # only INFO, WARNING, ERROR, and CRITICAL logs will show

def lambda_handler(event, context):
    for record in event["Records"]:
        reportmetadata = record["body"]["reportMetadata"]
        ctc = reportmetadata["sheetConfiguration"]["commonTableConfiguration"]
        ptcs = reportmetadata["sheetConfiguration"]["pivotTableConfigurations"]
        tablename = ctc["name"]
        cid = record["body"]["cid"]
        token = record["body"]["token"]
        queryendpoint = record["body"]["queryEndpoint"]

        tablemetadata = fetch_viewable_table_metadata(queryendpoint, cid, token)

        # check if the metadata response is empty or unviewable
        if not tablemetadata or not tablemetadata.get(tablename):
            logger.error("Unable to access table: " + tablename)

        requestedcols = set(ctc["columns"])
        tablecolumns = dict()
        for column in tablemetadata[tablename]:
            # only add columns that the user requested
            if column["name"] in requestedcols:
                tablecolumns[column["name"]] = column

        dataframe = fetch_table_rows(queryendpoint, cid, tablename, tablecolumns, ctc["columnValueFilter"], token)
        dataframe_to_file(reportmetadata["id"], tablename, dataframe, rawdatapath)
        try:
            for ptc in ptcs:
                values = None if not ptc["values"] else ptc["values"]
                rowgroups = None if not ptc["rowGroups"] else ptc["rowGroups"]
                columngroups = None if not ptc["columnGroups"] else ptc["columnGroups"]
                aggf = agg[ptc["calculatedBy"].lower()]
                table = pd.pivot_table(dataframe, values=values, index=rowgroups, columns=columngroups, aggfunc=aggf, margins=True).fillna(0)
                framejson = dataframe_to_json(table)
                reportjson = create_report_item(reportmetadata["id"], ptc["name"], tablename, current_milli_time(), framejson)
                load_data("dt-service-app-svcs-reporting-reports", reportjson)
                update_report_last_build_success(reportmetadata["id"], current_datetime_time())
        except BaseException as e:
            logger.error("Error occured generating the pivot table")
            logger.error(e)
            update_report_last_build_failure(reportmetadata["id"])

# returns a dictionary containing the table names and a list of column names that are viewable
def fetch_viewable_table_metadata(endpoint, cid, token):
    headers = prep_headers(cid, token)
    json = send_request("GET", endpoint, headers, None)
    metadatas = dict()
    for table in json["data"]:
        name = table["name"]
        metadatas[name] = table["columns"]
    return metadatas

# returns a pandas dataframe containing the requested table data
def fetch_table_rows(endpoint, cid, tablename, columns, filters, token):
    datacolumns = dict()
    columnnames = list(columns.keys())
    for columnname in columnnames:
        datacolumns[columnname] = []
    headers = prep_headers(cid, token)
    cursor = ""
    numrequests = 0
    while True:
        # TODO remove this numrequests check when filters is implemented by datasources
        if numrequests > 3:
            break
        body = prep_table_query(tablename, columnnames, 100, cursor, [], [])
        json = send_request("POST", endpoint, headers, body)
        for row in json["rows"]:
            for col_name in columnnames:
                coltype = columns[col_name]["type"]
                # sometimes the decimal and integer values from the datasource is returned as a string
                value = float(row[col_name]) if coltype == "DECIMAL" or coltype == "INTEGER" else row[col_name]
                datacolumns[col_name].append(value)
        if not json.get("cursor") or not json["cursor"]:
            break
        cursor = json["cursor"]
        numrequests += 1
    logger.info("Number of requests made for table %s: %d", tablename, numrequests)
    return pd.DataFrame(datacolumns)

# returns a dictionary with the required request headers
def prep_headers(cid, token):
    return {"dc-cid": cid, "Authorization": "Bearer " + token}

# returns a dictionary containing the data source query
def prep_table_query(tablename, columnnames, limit, cursor, filters, orders):
    return {
        "table": {
            "name": tablename,
            "columns": columnnames
        },
        "filter": {
            "limit": limit,
            "cursor": cursor,
            "filters": filters
        },
        "sort": {
            "orders": orders
        }
    }

# executes a request and returns the json response if successful, raises a RuntimeException if not successful
def send_request(method, url, headers, json):
    try:
        r = requests.request(method=method, url=url, headers=headers, json=json)
        if not(r.ok):
            logger.error("Request failed with status code: %d", r.status_code)
            raise RuntimError("Request failed with non-2XX status code")
        return r.json()
    except (requests.RequestException, requests.HTTPError, ConnectionError):
        logger.error("An error occurred attempting %s request for url: %s", method, url)
        raise RuntimeError("Failed to execute request")
    except requests.JSONDecodeError:
        logger.error("An error occured decoding JSON response of %s request for url: %s", method, url)
        raise RuntimeError("Failed to decode JSON response")
    except BaseException as err:
        logger.error("An unexpected error type %s occurred attempting %s request for url: %s", type(err), method, url)
        raise RuntimeError("Failed to execute request with unexpected error")
