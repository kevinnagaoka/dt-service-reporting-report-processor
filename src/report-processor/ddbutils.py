import boto3
import json
from decimal import Decimal

def dataframe_to_json(dataframe):
    # for v1 with no nested tables, flatten the columns list
    columns = list(map(lambda cols: cols[1], dataframe.columns.tolist()))
    rows = dataframe.values.tolist()
    index = dataframe.index.tolist()
    return {
        "columns": columns,
        "rows": rows,
        "index": index
    }

def create_report_item(reportid, sheetname, tablename, lastmodified, table):
    return {
        "report_id": str(reportid),
        "sheet_name": sheetname,
        "table_name": tablename,
        "last_modified": lastmodified,
        "table": table
    }

def load_data(tablename, data):
    ddb = boto3.resource("dynamodb")
    table = ddb.Table(tablename)
    # convert all float values to Decimals to prevent boto3 errors
    ddbitem = json.loads(json.dumps(data), parse_float=Decimal)
    table.put_item(Item=ddbitem)